$(function () {
    $("[data-toggle = 'tooltip']").tooltip();
    $("[data-toggle ='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
});

$('#contacto').on('show.bs.modal', function (e) {
    console.log('se va a mostrar el modal');
    
    $("#contactoBtn").removeClass('btn-outline-success');
    $("#contactoBtn").addClass('btn-primary');
    $("#contactoBtn").prop('disabled', true);
})

$('#contacto').on('shown.bs.modal', function (e) {
    console.log('se mostró el modal');
})

$('#contacto').on('hide.bs.modal', function (e) {
    console.log('se ocultara el modal');
    $(".btnModal").removeClass('btn-outline-danger')
    $(".btnModal").prop('disabled', false)
})
$('#contacto').on('hidden.bs.modal', function (e) {
    console.log('se oculto el modal');
})